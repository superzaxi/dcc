# ----- vars -----
SumoDirName="sumo-1.7.0"

# ----- install commands of SUMO (sumo, sumo-gui) -----
brew update
brew tap dlr-ts/sumo
brew cask install xquartz
brew cask install sumo-gui
brew install sumo
brew install cmake
brew install xerces-c fox proj gdal gl2ps

# ----- build SUMO -----
export SUMO_HOME="$PWD/$SumoDirName"
cd $SUMO_HOME
mkdir build/cmake-build
cd build/cmake-build
cmake ../..

cd $SUMO_HOME/build/cmake-build
cmake --build . --parallel $(sysctl -n hw.ncpu)
