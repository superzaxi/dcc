import sys
import re

delayCount = 0
delayValue = 0
CBRCount = 0
CBRValue = 0
sendCAMCount = 0
sendCPMCount = 0
receiveCAMCount = 0
receiveCPMCount = 0

with open(sys.argv[1]) as f:
    for s_line in f:
        key, value = s_line.split()
        if(key == 'delay'):
            delayCount += 1
            delayValue += int(value)
        elif(key == 'CBR'):
            CBRCount += 1
            CBRValue += float(value)
        elif(key == 'sendCAM'):
            sendCAMCount += 1
        elif(key == 'sendCPM'):
            sendCPMCount += 1
        elif(key == 'receiveCAM'):
            receiveCAMCount += 1
        elif(key == 'receiveCPM'):
            receiveCPMCount += 1

CAMRatio = float(receiveCAMCount)/float(sendCAMCount)
CPMRatio = float(receiveCPMCount)/float(sendCPMCount)

print("averageDelay: " + str(float(delayValue)/float(delayCount)))
print("averageCBR: " + str(float(CBRValue)/float(CBRCount)))
print("averageCAMCPMRatio: " + str(CPMRatio/(CAMRatio+CPMRatio)))
#print("sendCAMCount: " + str(sendCAMCount))
