#Instance general
#Component Simulation
seed = 123
mobility-seed = 123
simulation-time = 30.000000000
time-step-event-synchronization-step = 0.100000000
trace-output-mode = Text
trace-index-output = true
trace-output-file = simulation.trace
statistics-output-file = simulation.stat
statistics-output-for-no-data = true
allow-node-re-creation = false
network-terminate-sim-when-routing-fails = false
progress-sim-time-output-interval-percents = 5.000000000
enable-unused-parameter-warnings = true

#Component GIS
gis-road-driving-side = Left
gis-los-break-down-cureved-road-into-straight-roads = true
gis-number-entrances-to-building = 0
gis-number-entrances-to-station = 0
gis-number-entrances-to-busstop = 0
gis-number-entrances-to-park = 0
gis-road-set-intersection-margin = false

#Component Antenna/Propagation
number-data-parallel-threads-for-propagation = 1
custom-antenna-file = customAntenna.ant
antenna-pattern-two-2d-to-3d-interpolation-algorithm-number = 1
antenna-patterns-are-in-legacy-format = false

#Component Dot11
dot11-phy-use-short-guard-interval-and-shrink-ofdm-symbol-duration = false

#Component Dot11Ad
dot11ad-bit-error-rate-curve-file = dot11admodes.ber

#Component Dot11Ah


[102000001] is-member-of = BuildingObjectType
[1-2] is-member-of = Dot11adObjectType
#Instance general

#Instance 60GHzBand
#Component Channel
[60GHzBand] channel-count = 2
[60GHzBand] channel-0-frequency-mhz = 58320.000000000
[60GHzBand] channel-0-bandwidth-mhz = 2160.000000000
[60GHzBand] channel-1-frequency-mhz = 60480.000000000
[60GHzBand] channel-1-bandwidth-mhz = 2160.000000000
[60GHzBand] propagation-enable-mask-calculated-channel-interference = false
[60GHzBand] propagation-model = TwoRayGround
[60GHzBand] enable-propagation-delay = true
[60GHzBand] max-signal-propagation-meters = 100000000.000000000
[60GHzBand] propagation-allow-multiple-interfaces-on-same-channel = false
[60GHzBand] fading-model = Off


#Instance dot11ad
#Component Antenna/Propagation (Interface)
[1-2;dot11ad] channel-instance-id = 60GHzBand
[1-2;dot11ad] antenna-model = Custom
[1-2;dot11ad] antenna-model-use-quasi-omni-mode = true
[1-2;dot11ad] antenna-model-quasi-omni-mode-gain-dbi = 0.000000000
[1-2;dot11ad] antenna-height-meters = 1.500000000
[1-2;dot11ad] antenna-azimuth-degrees = 0.000000000
[1-2;dot11ad] antenna-elevation-degrees = 0.000000000
[1-2;dot11ad] antenna-offset-meters = 0.000000000
[1-2;dot11ad] antenna-offset-degrees = 0.000000000

#Component Routing

#Component Network (Interface)
[1-2;dot11ad] network-address = 90.6.0.0 + $n
[1-2;dot11ad] network-prefix-length-bits = 16
[1-2;dot11ad] network-subnet-is-multihop = false
[1-2;dot11ad] network-address-is-primary = false
[1-2;dot11ad] network-allow-routing-back-out-same-interface = true
[1-2;dot11ad] network-ignore-unregistered-protocol = false
[1-2;dot11ad] network-mtu-bytes = 1500
[1-2;dot11ad] mac-protocol = Dot11ad
[1-2;dot11ad] interface-output-queue-max-packets-per-subq = 1000
[1-2;dot11ad] interface-output-queue-max-bytes-per-subq = 1000000
[1-2;dot11ad] network-enable-dhcp-client = false
[1-2;dot11ad] network-enable-dhcp-server = false
[1-2;dot11ad] network-enable-ndp = false
[1-2;dot11ad] network-enable-arp = false

#Component Dot11Mac
[1;dot11ad] dot11-node-type = Access-Point
[2;dot11ad] dot11-node-type = Mobile-STA
[2;dot11ad] dot11-channel-scan-interval = 0.500000000
[2;dot11ad] dot11-channel-scan-start-time-max-jitter = 0.500000000
[2;dot11ad] dot11-association-threshold-rssi-dbm = -68.000000000
[2;dot11ad] dot11-disassociation-threshold-rssi-dbm = -71.000000000
[2;dot11ad] dot11-associate-failure-timeout-interval = 1.000000000
[2;dot11ad] dot11-beacon-rssi-moving-average-coefficient = 0.500000000
[2;dot11ad] dot11-authentication-timeout-interval = 1.000000000
[1;dot11ad] dot11-access-point-auth-processing-delay = 0.010000000
[1-2;dot11ad] dot11-map-ip-multicast-addresses = false
[1-2;dot11ad] dot11-enable-high-throughput-mode = false
[1-2;dot11ad] dot11-adaptive-rate-control-type = Static
[1-2;dot11ad] dot11-modulation-and-coding = Dot11ad-MCS4
[1-2;dot11ad] dot11-modulation-and-coding-for-management-frames = Dot11ad-MCS4
[1-2;dot11ad] dot11-modulation-and-coding-for-broadcast = Dot11ad-MCS4
[1-2;dot11ad] dot11-ack-datarate-selection-type = SameAsData
[1-2;dot11ad] dot11-ack-datarate-match-num-spatial-streams = false
[1-2;dot11ad] dot11-short-frame-retry-limit = 7
[1-2;dot11ad] dot11-long-frame-retry-limit = 4
[1;dot11ad] dot11-ap-scheduling-algorithm = FIFO
[1-2;dot11ad] dot11-max-aggregate-mpdu-size-bytes = 65535
[1-2;dot11ad] dot11-max-num-aggregate-subframes = 64
[1-2;dot11ad] dot11-protect-aggregate-frames-with-single-acked-frame = true
[1-2;dot11ad] dot11-allow-frame-aggregation-with-txop-zero = false
[1-2;dot11ad] dot11-contention-window-min-slots = 15
[1-2;dot11ad] dot11-contention-window-max-slots = 1023
[1-2;dot11ad] dot11-disabled-to-jump-on-medium-without-backoff = false
[1-2;dot11ad] dot11-qos-type = EDCA
[1-2;dot11ad] dot11-edca-category-0-priority-list = 0
[1-2;dot11ad] dot11-edca-category-0-num-aifs-slots = 7
[1-2;dot11ad] dot11-edca-category-0-contention-window-min-slots = 15
[1-2;dot11ad] dot11-edca-category-0-contention-window-max-slots = 1023
[1-2;dot11ad] dot11-edca-category-0-frame-lifetime = inf_time
[1-2;dot11ad] dot11-edca-category-0-downlink-txop-duration = 0.000300000
[1-2;dot11ad] dot11-edca-category-0-max-non-fifo-aggregate-size-bytes = 0
[1-2;dot11ad] dot11-edca-category-1-priority-list = 1
[1-2;dot11ad] dot11-edca-category-1-num-aifs-slots = 3
[1-2;dot11ad] dot11-edca-category-1-contention-window-min-slots = 15
[1-2;dot11ad] dot11-edca-category-1-contention-window-max-slots = 1023
[1-2;dot11ad] dot11-edca-category-1-frame-lifetime = inf_time
[1-2;dot11ad] dot11-edca-category-1-downlink-txop-duration = 0.000300000
[1-2;dot11ad] dot11-edca-category-1-max-non-fifo-aggregate-size-bytes = 0
[1-2;dot11ad] dot11-edca-category-2-priority-list = 2
[1-2;dot11ad] dot11-edca-category-2-num-aifs-slots = 2
[1-2;dot11ad] dot11-edca-category-2-contention-window-min-slots = 7
[1-2;dot11ad] dot11-edca-category-2-contention-window-max-slots = 15
[1-2;dot11ad] dot11-edca-category-2-frame-lifetime = inf_time
[1-2;dot11ad] dot11-edca-category-2-downlink-txop-duration = 0.000300000
[1-2;dot11ad] dot11-edca-category-2-max-non-fifo-aggregate-size-bytes = 0
[1-2;dot11ad] dot11-edca-category-3-priority-list = 3
[1-2;dot11ad] dot11-edca-category-3-num-aifs-slots = 2
[1-2;dot11ad] dot11-edca-category-3-contention-window-min-slots = 3
[1-2;dot11ad] dot11-edca-category-3-contention-window-max-slots = 7
[1-2;dot11ad] dot11-edca-category-3-frame-lifetime = inf_time
[1-2;dot11ad] dot11-edca-category-3-downlink-txop-duration = 0.000300000
[1-2;dot11ad] dot11-edca-category-3-max-non-fifo-aggregate-size-bytes = 0

#Component Dot11Phy
[1-2;dot11ad] dot11-phy-protocol = IEEE802.11
[1-2;dot11ad] dot11-tx-power-specified-by = PhyLayer
[1-2;dot11ad] dot11-tx-power-dbm = 20.000000000
[1-2;dot11ad] dot11-radio-noise-figure-db = 10.000000000
[1-2;dot11ad] dot11-preamble-detection-power-threshold-dbm = -68.000000000
[1-2;dot11ad] dot11-energy-detection-power-threshold-dbm = -48.000000000
[1-2;dot11ad] dot11-signal-capture-ratio-threshold-db = 1000.000000000
[1-2;dot11ad] dot11-ofdm-symbol-duration = 0.000004000
[1-2;dot11ad] dot11-slot-time = 0.000005000
[1-2;dot11ad] dot11-sifs-time = 0.000003000
[1-2;dot11ad] dot11-rx-tx-turnaround-time = 0.000000800
[1-2;dot11ad] dot11-phy-rx-start-delay = 0.000003600
[1-2;dot11ad] dot11-preamble-length-duration = 0.000001891
[1-2;dot11ad] dot11-short-training-field-duration = 0.000001236
[1-2;dot11ad] dot11-plcp-header-length-duration = 0.000000582
[1-2;dot11ad] dot11-phy-high-throughput-header-additional-duration = 0.000012000
[1-2;dot11ad] dot11-phy-high-throughput-header-additional-per-stream-duration = 0.000004000

#Component Dot11AdMac
[2;dot11ad] dot11ad-forced-ap-pcp-nodeid = 1
[1-2;dot11ad] dot11ad-number-directional-sectors = 36
[1-2;dot11ad] dot11ad-beamforming-sector-selector-scheme-name = RSSI
[1-2;dot11ad] dot11ad-abft-max-num-responder-txss-frames = 36
[1-2;dot11ad] dot11ad-ap-receive-sector-sweep-interval-to-beacon = 0
[1-2;dot11ad] dot11ad-sta-receive-sector-sweep-interval-to-beacon = 0
[1-2;dot11ad] dot11ad-beacon-superframe-interval-duration = 0.100000000
[1-2;dot11ad] dot11ad-beacon-transmission-interval-aka-bti-duration = 0.001000000
[1-2;dot11ad] dot11ad-association-beamforming-training-aka-abft-duration = 0.004000000
[1-2;dot11ad] dot11ad-data-transfer-interval-1-relative-start-time = 0.000000000
[1-2;dot11ad] dot11ad-data-transfer-interval-1-duration = 0.095000000
[1-2;dot11ad] dot11ad-data-transfer-interval-1-is-contention-based-access-period = true
[1-2;dot11ad] dot11ad-short-beamforming-interframe-space-duration = 0.000001000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs0 = 27.500000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs1 = 385.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs2 = 770.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs3 = 962.500000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs4 = 1155.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs5 = 1251.250000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs6 = 1540.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs7 = 1925.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs8 = 2310.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs9 = 2502.500000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs10 = 3080.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs11 = 3850.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs12 = 4620.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs13 = 693.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs14 = 866.250000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs15 = 1386.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs16 = 1732.500000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs17 = 2079.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs18 = 2772.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs19 = 3465.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs20 = 4158.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs21 = 4504.500000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs22 = 5197.500000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs23 = 6237.000000000
[1-2;dot11ad] dot11ad-mbps-datarate-for-dot11ad-mcs24 = 6756.750000000


#Instance general
#Component Common

#Component Position

#Component CommunicationObject

#Component SimulationObject
[1-2] trace-enabled-tags = Mac Phy
[1-2,102000001] trace-start-time = 0.000000000

#Component GisObject
[102000001] gisobject-disable-time = inf_time
[102000001] gisobject-enable-time = inf_time
[102000001] gisobject-elevation-reference-type = GroundLevel

#Component Building

#Component Network (Node)
[1-2] network-hop-limit = 64
[1-2] network-loopback-delay = 0.000000001

#Component Transport

#Component Mobility
[1] mobility-model = Stationary
[2] mobility-model = Random-Waypoint
[1-2] mobility-granularity-meters = 1.000000000
[2] mobility-wp-pause-time = 0.000000000
[2] mobility-wp-min-speed-meter-per-sec = 1.000000000
[2] mobility-wp-max-speed-meter-per-sec = 1.000000000
[2] mobility-rwp-movable-area-is-rect = false
[2] mobility-rwp-movable-area-gis-object-name = Building1
[1-2] mobility-init-positions-file = simulation.pos
[1-2] mobility-need-to-add-ground-height = true


#Instance IperfUdp1

gis-object-position-in-latlong-degree = false
gis-object-file-path = shapes/
gis-object-files = building.shp

#Component IperfUdp
[1;IperfUdp1] iperf-udp-destination = 2
[1;IperfUdp1] iperf-udp-start-time = 5.000000000
[1;IperfUdp1] iperf-udp-time-mode = true
[1;IperfUdp1] iperf-udp-total-time = 20.000000000
[1;IperfUdp1] iperf-udp-payload-size-bytes = 1470
[1;IperfUdp1] iperf-udp-rate-bps = 1500000000
[1;IperfUdp1] iperf-udp-use-system-time = false
[1;IperfUdp1] iperf-udp-priority = 0
[1;IperfUdp1] iperf-udp-auto-address-mode = true
[1;IperfUdp1] iperf-udp-auto-port-mode = true
[1;IperfUdp1] iperf-udp-use-virtual-payload = true


statistics-configuration-file = simulation.statconfig
