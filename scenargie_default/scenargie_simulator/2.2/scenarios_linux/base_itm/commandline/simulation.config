#Instance general
#Component Simulation
seed = 123
mobility-seed = 123
simulation-time = 120.000000000
time-step-event-synchronization-step = 0.100000000
trace-output-mode = Text
trace-index-output = true
trace-output-file = simulation.trace
statistics-output-file = simulation.stat
statistics-output-for-no-data = true
allow-node-re-creation = false
network-terminate-sim-when-routing-fails = false
progress-sim-time-output-interval-percents = 5.000000000
enable-unused-parameter-warnings = true

#Component GIS
gis-road-driving-side = Left
gis-los-break-down-cureved-road-into-straight-roads = true
gis-number-entrances-to-building = 0
gis-number-entrances-to-station = 0
gis-number-entrances-to-busstop = 0
gis-number-entrances-to-park = 0
gis-road-set-intersection-margin = false

#Component Antenna/Propagation
number-data-parallel-threads-for-propagation = 1
antenna-pattern-two-2d-to-3d-interpolation-algorithm-number = 1
antenna-patterns-are-in-legacy-format = false


[100000001-100000012] is-member-of = RoadObjectType
[1-3] is-member-of = AlohaObjectType
#Instance general

#Instance 5GHzBand
#Component Channel
[5GHzBand] channel-frequency-mhz = 5000.000000000
[5GHzBand] channel-bandwidth-mhz = 20.000000000
[5GHzBand] propagation-enable-mask-calculated-channel-interference = false
[5GHzBand] propagation-model = ITM
[5GHzBand] propitm-calculation-point-division-length = 1.000000000
[5GHzBand] propitm-earth-dielectric-constant = 15.000000000
[5GHzBand] propitm-earth-conductivity = 0.005000000
[5GHzBand] propitm-atmospheric-bending-constant = 350.000000000
[5GHzBand] propitm-fraction-of-time = 0.500000000
[5GHzBand] propitm-fraction-of-situations = 0.500000000
[5GHzBand] propitm-radio-climate = Maritime-Temperate-Over-Land
[5GHzBand] propitm-polarization = Horizontal
[5GHzBand] propitm-enable-foliage-loss = false
[5GHzBand] propitm-enable-vertical-diffraction-path-calculation = false
[5GHzBand] enable-propagation-delay = true
[5GHzBand] max-signal-propagation-meters = 100000000.000000000
[5GHzBand] propagation-allow-multiple-interfaces-on-same-channel = false
[5GHzBand] fading-model = Off


#Instance general
#Component Common

#Component Position

#Component CommunicationObject

#Component SimulationObject
[1-3] trace-enabled-tags = Application Mac Phy
[1-3,100000001-100000012] trace-start-time = 0.000000000

#Component GisObject
[100000001-100000012] gisobject-disable-time = inf_time
[100000001-100000012] gisobject-enable-time = inf_time
[100000001-100000012] gisobject-elevation-reference-type = GroundLevel

#Component Road

#Component Network (Node)
[1-3] network-hop-limit = 64
[1-3] network-loopback-delay = 0.000000001

#Component Transport

#Component Mobility
[1] mobility-model = Stationary
[2-3] mobility-model = Gis-Based-Random-Waypoint
[1-3] mobility-granularity-meters = 1.000000000
[2-3] mobility-wp-pause-time = 0.000000000
[2-3] mobility-wp-min-speed-meter-per-sec = 15.000000000
[2-3] mobility-wp-max-speed-meter-per-sec = 20.000000000
[2-3] mobility-gis-ground-object-type = Road
[1-3] mobility-init-positions-file = simulation.pos
[2-3] mobility-lane-offset-meters = 4.000000000
[2-3] mobility-route-search-based-algorithm = false
[1] mobility-need-to-add-ground-height = true


#Instance aloha
#Component Antenna/Propagation (Interface)
[1-3;aloha] channel-instance-id = 5GHzBand
[1-3;aloha] antenna-model = Omnidirectional
[1-3;aloha] antenna-gain-dbi = 0.000000000
[1-3;aloha] antenna-height-meters = 1.500000000
[1-3;aloha] antenna-azimuth-degrees = 0.000000000
[1-3;aloha] antenna-elevation-degrees = 0.000000000
[1-3;aloha] antenna-offset-meters = 0.000000000
[1-3;aloha] antenna-offset-degrees = 0.000000000

#Component Routing

#Component Network (Interface)
[1-3;aloha] network-address = 90.0.0.0 + $n
[1-3;aloha] network-prefix-length-bits = 16
[1-3;aloha] network-subnet-is-multihop = false
[1-3;aloha] network-address-is-primary = false
[1-3;aloha] network-allow-routing-back-out-same-interface = true
[1-3;aloha] network-ignore-unregistered-protocol = false
[1-3;aloha] network-mtu-bytes = 1500
[1-3;aloha] mac-protocol = ALOHA
[1-3;aloha] interface-output-queue-max-packets = 1000
[1-3;aloha] interface-output-queue-max-bytes = 1000000
[1-3;aloha] network-enable-dhcp-client = false
[1-3;aloha] network-enable-dhcp-server = false
[1-3;aloha] network-enable-ndp = false
[1-3;aloha] network-enable-arp = false

#Component AlohaMac
[1-3;aloha] aloha-model = Unslotted
[1-3;aloha] aloha-datarate-bits-per-second = 1000000
[1-3;aloha] aloha-tx-power-dbm = 20.000000000
[1-3;aloha] aloha-minimum-data-transmission-interval = 0.005000000
[1-3;aloha] aloha-maximum-data-transmission-jitter = 0.001000000
[1-3;aloha] aloha-minimum-retry-interval = 0.010000000
[1-3;aloha] aloha-maximum-retry-interval = 0.100000000
[1-3;aloha] aloha-retry-limit = 10

#Component AlohaPhy
[1-3;aloha] aloha-signal-rx-power-threshold-dbm = -85.000000000
[1-3;aloha] aloha-phy-frame-data-padding-bits = 0
[1-3;aloha] aloha-phy-delay-until-airborne = 0.000020000


#Instance CBR1

gis-object-position-in-latlong-degree = false
gis-object-file-path = shapes/
gis-object-files = road.shp trafficlight.shp busstop.shp ground.shp

#Component CBR
[1;CBR1] cbr-destination = *
[1;CBR1] cbr-start-time = 10.000000000
[1;CBR1] cbr-end-time = 110.000000000
[1;CBR1] cbr-start-time-max-jitter = 1.000000000
[1;CBR1] cbr-payload-size-bytes = 128
[1;CBR1] cbr-traffic-defined-by = Interval
[1;CBR1] cbr-interval = 1.000000000
[1;CBR1] cbr-priority = 0
[1;CBR1] cbr-auto-port-mode = true
[1;CBR1] cbr-use-virtual-payload = false


statistics-configuration-file = simulation.statconfig
