// Copyright (c) 2007-2017 by Space-Time Engineering, LLC ("STE").
// All Rights Reserved.
//
// This source code is a part of Scenargie Software ("Software") and is
// subject to STE Software License Agreement. The information contained
// herein is considered a trade secret of STE, and may not be used as
// the basis for any other software, hardware, product or service.
//
// Refer to license.txt for more specific directives.

#ifndef BLE_EXTENSION_CHOOSER_H
#define BLE_EXTENSION_CHOOSER_H

#ifdef BLE
    #include "ble_application.h"
    #include "ble_host.h"
#else
    #include "ble_extension_notavailable.h"
#endif

#endif
