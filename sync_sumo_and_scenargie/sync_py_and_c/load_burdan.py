# coding: utf-8
import time
import threading
import requests

count = 0
start_time = time.time()
thread_num = 1

uri = "http://127.0.0.1:7900/packet"


def main():
    try:
        global count
        global start_time

        res = conduct_func()
        while res.status_code == 200:
            res = conduct_func()
            count = count + 1
    except Exception:
        pass


def print_info():
    try:
        res = conduct_func()
        while res.status_code == 200:
            time.sleep(1)
            res = conduct_func()
            print("throughput: " + str(count / (time.time() - start_time)))
            print("total time(sec): " + str(time.time() - start_time))
            print("total load: " + str(count))

    except Exception:
        pass


def conduct_func():
    return requests.get(uri)


try:
    threads = []

    for i in range(0, thread_num):
        threads.append(threading.Thread(target=main))
    threads.append(threading.Thread(target=print_info))

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    print("total time(sec): " + str(time.time() - start_time))
    print("total load: " + str(count))

except Exception as e:
    print(e)
    print("total time(sec): " + str(time.time() - start_time))
    print("total load: " + str(count))
