# coding utf-8
from copy import deepcopy

access_log = {
    "time": 0,
    "packet": 0,
    "delete_vehicle": 0,
    "add_vehicle": 0,
    "finish": 0,
    "vehicle": 0,
}

if __name__ == "__main__":
    with open("./access.log") as f:
        for line in f:
            is_updated = False

            for k in access_log.keys():
                if k in line:
                    is_updated = True
                    access_log[k] = access_log[k] + 1
                    break
                else:
                    continue

            if is_updated == False and "127.0.0.1" in line:
                print(line)

    print(access_log)
