#include <iostream>
#include "my_headers.h"

// ##### GLOBAL VARS #####
int SIMULATION_TIME = 1000;
static std::string HOST = "127.0.0.1";
int PORT = 8000;

// ##### function #####
void send_current_time(int current_time) {
  // ----- premise for lua -----
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  luaL_loadfile(L, "test.lua");
  lua_pcall(L,0,0,0);

  // ----- call function in lua code. -----
  lua_getglobal(L, "send_current_time");
  lua_pushnumber(L, current_time);
  lua_pcall(L, 1, 0, 0);
}

void wait_until_sync(int current_time) {
  // ----- premise for lua -----
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  luaL_loadfile(L, "test.lua");
  lua_pcall(L,0,0,0);

  // ----- call function in lua code. -----
  lua_getglobal(L, "wait_until_sync");
  lua_pushnumber(L, current_time);
  lua_pcall(L, 1, 0, 0);
}

// ##### main #####
int main(){
  for ( int t = 0; t < SIMULATION_TIME; t++){
    send_current_time(t);
    printf("time : %d\n", t);
    wait_until_sync(t);
  }

  return 0;
}
