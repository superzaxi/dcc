# coding : utf-8

import falcon
import json


from common import (
    Common
)

class PacketHandler(Common):
    def on_get(self, req, res):
        try:
            res.body = json.dumps(self.data)
            res.status = falcon.HTTP_200

            # ----- remove redundant data -----
            self.remove_redundant_data_by_time(2)

        except Exception as e:
            print(e)
            res.status = falcon.HTTP_500
