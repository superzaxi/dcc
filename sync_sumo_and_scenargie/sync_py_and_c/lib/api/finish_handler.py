# coding : utf-8

import falcon
import json

from common import (
    Common
)


class FinishHandler(Common):
    def on_get(self, req, res):
        try:
            res.body = json.dumps({"is_finish": int(self.is_finish())})
            res.status = falcon.HTTP_200
        except Exception as e:
            print(e)
            res.body = json.dumps({"is_finish": 0})
            res.status = falcon.HTTP_500

    def is_finish(self):
        if 0 < max([0] + [d["is_finish"] for d in self.data]):
            return True
        else:
            return False
