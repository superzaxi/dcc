# coding : utf-8

import falcon
import json

from functools import reduce

from common import (
    Common
)


class VehicleHandler(Common):
    def on_get(self, req, res):
        try:
            params = req.params
            data_with_node_id = [d for d in self.data if str(d["id"]) == str(params["node_id"])]

            if len(data_with_node_id) <= 0:
                res.body = json.dumps(self.get_vehicle_info(
                    0,
                    0,
                    "null",
                    "null",
                    0,
                    0,
                    0,
                    0
                ))
                res.status = falcon.HTTP_406
            else:
                the_data = reduce(lambda a, b: b if float(a["time"]) <= float(b["time"]) else a, data_with_node_id)

                res.body = json.dumps(self.get_vehicle_info(
                    the_data["id"],
                    the_data["time"],
                    the_data["road"],
                    the_data["pos"],
                    self.parse_pos(the_data["pos"])[0],
                    self.parse_pos(the_data["pos"])[1],
                    the_data["speed"],
                    the_data["accel"]
                ))
                res.status = falcon.HTTP_200

        except Exception as e:
            print(e)
            res.status = falcon.HTTP_500

    def parse_pos(self, pos):
        return self.float_from_str(pos.split(",")[0]), self.float_from_str(pos.split(",")[1])

    def float_from_str(self, str):
        split_str = str.split(".")

        if len(split_str) <= 1:
            return float(self.num_str_from_str(split_str[0]))
        else:
            return float(self.num_str_from_str(split_str[0]) + "." + self.num_str_from_str(split_str[1]))

    def num_str_from_str(self, str):
        return reduce(lambda a, b: a + b if b.isdigit() else a, str, "")

    def get_vehicle_info(self, node_id, time, road, pos, pos_x, pos_y, speed, accel):
        vehicle_info = {
            "id": node_id,
            "time": time,
            "road": road,
            "pos": pos,
            "pos_x": pos_x,
            "pos_y": pos_y,
            "speed": speed,
            "accel": accel
        }

        return vehicle_info
