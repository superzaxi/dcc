# coding : utf-8

import falcon
import json


class Common:
    def __init__(self):
        self.data = []

    def on_get(self, req, res):
        try:
            res.body = json.dumps(self.data)
            res.status = falcon.HTTP_200

        except Exception as e:
            print(e)
            res.status = falcon.HTTP_500

    def on_post(self, req, res):
        try:
            body = req.bounded_stream.read()
            data = json.loads(body.decode('utf-8'))
            self.data.append(data)
            res.status = falcon.HTTP_200

        except Exception as e:
            print(e)
            res.status = falcon.HTTP_500

    def remove_redundant_data_by_time(self, diff_from_max_time=2):
        '''
            remove data whose time is less than max time by diff_from_max_time.
        '''
        try:
            max_time = max([float(d["time"]) for d in self.data])

            for i in range(0, len(self.data)):
                v = self.data.pop(0)

                if float(max_time) - float(v["time"]) <= diff_from_max_time:
                    self.data.append(v)
                else:
                    continue
        except Exception:
            pass
