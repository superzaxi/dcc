# coding : utf-8
import falcon
import multiprocessing

from wsgiref import simple_server

from lib.api.finish_handler import (
    FinishHandler,
)
from lib.api.packet_handler import (
    PacketHandler,
)
from lib.api.time_handler import (
    TimeHandler,
)
from lib.api.vehicle_handler import (
    VehicleHandler,
)
from lib.api.add_vehicle_handler import (
    AddVehicleHandler,
)
from lib.api.delete_vehicle_handler import (
    DeleteVehicleHandler,
)

# ----- GLOBAL VARS -----
HOST = "127.0.0.1"
PORT = 8000
PORT_FOR_PACKET = 7900
PORT_FOR_TIME = 7800

# ----- main -----
if __name__ == "__main__":
    # ----- app for 8000 port -----
    app = falcon.API()
    app.add_route("/add_vehicle", AddVehicleHandler())
    app.add_route("/delete_vehicle", DeleteVehicleHandler())
    app.add_route("/finish", FinishHandler())
    app.add_route("/vehicle", VehicleHandler())
    httpd = simple_server.make_server(HOST, PORT, app)

    # ----- app for 7900 port -----
    app_for_packet = falcon.API()
    app_for_packet.add_route("/packet", PacketHandler())
    httpd_for_packet = simple_server.make_server(HOST, PORT_FOR_PACKET, app_for_packet)

    # ----- app for 7800 port -----
    app_for_time = falcon.API()
    app_for_time.add_route("/time", TimeHandler())
    httpd_for_time = simple_server.make_server(HOST, PORT_FOR_TIME, app_for_time)

    try:
        # ----- app for 8000 port -----
        process_for_default = multiprocessing.Process(target=httpd.serve_forever)
        process_for_default.start()

        # ----- app for 7900 port -----
        process_for_packet = multiprocessing.Process(target=httpd_for_packet.serve_forever)
        process_for_packet.start()

        # ----- app for 7800 port -----
        process_for_time = multiprocessing.Process(target=httpd_for_time.serve_forever)
        process_for_time.start()

    except KeyboardInterrupt:
        process_for_default.terminate()
        process_for_packet.terminate()
        process_for_time.terminate()
