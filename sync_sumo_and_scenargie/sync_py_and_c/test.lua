-- library --
json = require "json"
client = require('httpclient').new()

-- GLOVAL VARS --
HOST = '127.0.0.1'
PORT = 8000

URI_ON_TIME = 'http://'..tostring(HOST)..':'..tostring(PORT)..'/time'

-- function --
function hello()
  print("Hello!")
end

function send_current_time(current_time)
  res = client:post(URI_ON_TIME, json.encode({name = "Scenargie", time = current_time}))

  if res.code ~= 200 then
    send_current_time(current_time)
  end
end

function wait_until_sync(current_time)
  res = client:get(URI_ON_TIME)
  body = json.decode(res.body)

  if res.code ~= 200 or body["time"] < current_time then
    wait_until_sync(current_time)
  end
end

hello()
-- for test --
send_current_time(1)
