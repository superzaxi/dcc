# coding: utf-8

#!/usr/bin/env python
# Eclipse SUMO, Simulation of Urban MObility; see https://eclipse.org/sumo
# Copyright (C) 2009-2020 German Aerospace Center (DLR) and others.
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
# This Source Code may also be made available under the following Secondary
# Licenses when the conditions for such availability set forth in the Eclipse
# Public License 2.0 are satisfied: GNU General Public License, version 2
# or later which is available at
# https://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
# SPDX-License-Identifier: EPL-2.0 OR GPL-2.0-or-later

# @file    runner.py
# @author  Lena Kalleske
# @author  Daniel Krajzewicz
# @author  Michael Behrisch
# @author  Jakob Erdmann
# @date    2009-03-26

"""
Tutorial for traffic light control via the TraCI interface.
This scenario models a pedestrian crossing which switches on demand.
"""
from __future__ import absolute_import
from __future__ import print_function

import os
import sys
import optparse
import subprocess
import json
import requests
import time

# the directory in which this script resides
THISDIR = os.path.dirname(__file__)


# we need to import python modules from the $SUMO_HOME/tools directory
# If the the environment variable SUMO_HOME is not set, try to locate the python
# modules relative to this script
if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")
import traci  # noqa
from sumolib import checkBinary  # noqa
import randomTrips  # noqa

from lib.common_library import (
    is_sync_finished,
    get_added_node_ids_in_scenargie,
    getVehicleInfo,
    send_current_time,
    send_deleted_vehicle_id,
    send_vehicle_state,
    send_is_finish,
    wait_until_sync
)
from lib.models.arrived_vehicles import (
    ArrivedVehicles
)
from lib.models.departured_vehicles import (
    DeparturedVehicles
)

def run():
    """execute the TraCI control loop"""
    arrived_vehicles = ArrivedVehicles()
    departured_vehicles = DeparturedVehicles()

    while True:
        traci.simulationStep()
        # ----- finish SUMO when Scenargie has already been finishd -----
        if is_sync_finished() :
            break

        # ----- inform vehicles' status to Scenargie -----
        arrived_vehicles.update()
        departured_vehicles.update()
        running_vehicles_ids = list(set(departured_vehicles.get_all()) - set(arrived_vehicles.get_all()))
        for running_vehicle_id in running_vehicles_ids:
            send_vehicle_state(running_vehicle_id)

        # ----- inform arrived vehicles' id to Scenergie -----
        for deleted_vehicle_id in traci.simulation.getArrivedIDList():
            send_deleted_vehicle_id(deleted_vehicle_id, traci.simulation.getTime())

        # ----- send current time by sec -----
        send_current_time()
        wait_until_sync()

    send_is_finish()
    sys.stdout.flush()
    traci.close()

def get_options():
    """define options for this script and interpret the command line"""
    optParser = optparse.OptionParser()
    optParser.add_option("--nogui", action="store_true",
                         default=False, help="run the commandline version of sumo")
    options, args = optParser.parse_args()
    return options


# this is the main entry point of this script
if __name__ == "__main__":
    # load whether to run with or without GUI
    options = get_options()

    # this script has been called from the command line. It will start sumo as a
    # server, then connect and run
    if options.nogui:
        sumoBinary = checkBinary('sumo')
    else:
        sumoBinary = checkBinary('sumo-gui')

    net = 'traci.net.xml'

    # this is the normal way of using traci. sumo is started as a
    # subprocess and then the python script connects and runs
    traci.start([sumoBinary, '-c', os.path.join('junction', 'junction.sumocfg')])
    run()
