import random

output = 'junction.rou.xml'
counter = 0
vehID = 1

mainRoute = 'edge0 edge1 edge2 edge3 edge4'
mergeRoute = 'edge00 edge01 edge2 edge3 edge4'
simTime = 600

with open(output, mode='w') as f:
    f.write('<?xml version="1.0" encoding="UTF-8"?>\n\n')

with open(output, mode='a') as f:
    f.write('<routes xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://sumo.dlr.de/xsd/routes_file.xsd">\n')
    while counter < simTime:
        #counter += random.randint(7,10)
        counter += random.randint(3,5)

        f.write('   <vehicle id="%d" depart="%f" departLane="random" departPos="random" departSpeed="max">\n' % (vehID,counter))
        f.write('        <route edges="%s"/>\n' % mainRoute)
        f.write('    </vehicle>\n')

        vehID += 1

        #if random.randint(1,10) < 3:
        if random.randint(1,10) < 7:
            f.write('   <vehicle id="%d" depart="%f" departLane="random" departPos="random" departSpeed="max">\n' % (vehID,counter))
            f.write('       <route edges="%s"/>\n' % mergeRoute)
            f.write('    </vehicle>\n')

            vehID += 1

    f.write('</routes>\n')

