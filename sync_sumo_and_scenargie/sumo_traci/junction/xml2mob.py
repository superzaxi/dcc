import subprocess
from lxml import etree
import sys

outputFile = 'output.mob'

def write2mob(step):
    id = step['id']
    time = float(step['time'])
    x = step['x']
    y = step['y']
    height = 0
    angle = int(float(step['angle']))
    slope = int(float(step['slope']))

    with open(outputFile, mode='a') as f:
        f.write('%s %f  %s  %s  %d %d %d\n' % (id, time, x, y, height, angle, slope))



def parseXML(input):
    tree = etree.parse(input)
    root = tree.getroot()
    
    for time in root.iter('timestep'):
        step = time.attrib
        for vehID in time:
            vehInfo = vehID.attrib
            step.update(vehInfo)
            write2mob(step)
        

if __name__ == "__main__":
    args = sys.argv
    if len(args) != 2:
        print("input Error")
        sys.exit()
    else:
        inputPath = args[1]

    with open(outputFile, mode='w'):
        pass

    parseXML(inputPath)

    cmd = 'sort -k 1n -k 2n {output} -o {output}'.format(output=outputFile)
    subprocess.call(cmd.split())