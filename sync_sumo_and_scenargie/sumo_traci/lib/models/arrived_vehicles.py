# coding: utf-8

import traci
from functools import (
    reduce
)

class ArrivedVehicles:
    def __init__(self):
        self.arrived_vehicles_at_time_step = {}

    def get_all(self):
        return list(reduce(lambda a, b: a + b, [veh_id for veh_id in self.arrived_vehicles_at_time_step.values()] , tuple()))

    def update(self):
        now = traci.simulation.getTime()
        self.arrived_vehicles_at_time_step[now] = traci.simulation.getArrivedIDList()
