# coding: utf-8

import traci
from functools import (
    reduce
)

class DeparturedVehicles:
    def __init__(self):
        self.departured_vehicles_at_time_step = {}

    def get_all(self):
        return list(reduce(lambda a, b: a + b, [veh_id for veh_id in self.departured_vehicles_at_time_step.values()] , tuple()))

    def update(self):
        now = traci.simulation.getTime()
        self.departured_vehicles_at_time_step[now] = traci.simulation.getDepartedIDList()
