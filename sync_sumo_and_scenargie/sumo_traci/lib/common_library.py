# coding: utf-8

from __future__ import absolute_import
from __future__ import print_function

import os
import sys
import optparse
import subprocess
import json
import requests
import time

if 'SUMO_HOME' in os.environ:
    tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
    sys.path.append(tools)
else:
    sys.exit("please declare environment variable 'SUMO_HOME'")
import traci  # noqa
from sumolib import checkBinary  # noqa

# ----- GLOVAL VARS -----
HOST = "127.0.0.1"
PORT = 8000
PORT_FOR_PACKET = 7900
PORT_FOR_TIME = 7800

URI_ON_ADD_VEHICLE = "http://" + str(HOST) + ":" + str(PORT) + "/add_vehicle"
URI_ON_DELETE_VEHICLE = "http://" + str(HOST) + ":" + str(PORT) + "/delete_vehicle"
URI_ON_FINISH = "http://" + str(HOST) + ":" + str(PORT) + "/finish"
URI_ON_TIME = "http://" + str(HOST) + ":" + str(PORT_FOR_TIME) + "/time"
URI_ON_VEHICLE = "http://" + str(HOST) + ":" + str(PORT) + "/vehicle"
URI_ON_PACKET = "http://" + str(HOST) + ":" + str(PORT_FOR_PACKET) + "/packet"
# ----- GLOVAL VARS end -----

# ----- function -----
def getVehicleInfo(vehID):
    pos = traci.vehicle.getPosition(vehID)
    road = traci.vehicle.getRoadID(vehID)
    speed = traci.vehicle.getSpeed(vehID)
    accel = traci.vehicle.getAcceleration(vehID)

    requests.post(URI_ON_VEHICLE, data=json.dumps({
        "id": str(vehID),
        "time": str(traci.simulation.getTime()),
        "road": str(road),
        "pos": str(pos),
        "speed": str('{:.3f}'.format(speed)),
        "accel": str('{:.3f}'.format(accel))
    }))

def send_vehicle_state(veh_id):
    getVehicleInfo(veh_id)

def get_added_node_ids_in_scenargie(uri=URI_ON_ADD_VEHICLE):
    while True:
        res = requests.get(uri)

        if res.status_code != 200:
            continue
        else:
            return res.json()

def is_sync_finished(uri=URI_ON_FINISH):
    while True:
        res = requests.get(uri)

        if res.status_code != 200:
            continue

        if int(res.json()["is_finish"]) == 1:
            return True
        else:
            return False

def send_current_time(uri=URI_ON_TIME):
    while True:
        res = requests.post(uri, data=json.dumps({"name": "SUMO", "time": str(traci.simulation.getTime())}))
        if res.status_code == 200:
            break
        else:
            continue

def send_deleted_vehicle_id(vehicle_id, delete_time, uri=URI_ON_DELETE_VEHICLE):
    while True:
        res = requests.post(uri, data=json.dumps({"node_id": str(vehicle_id), "time": str(delete_time)}))
        if res.status_code == 200:
            break
        else:
            continue

def send_is_finish(uri=URI_ON_FINISH):
    while True:
        res = requests.post(uri, json.dumps({"name": "SUMO", "is_finish": 1}))
        if res.status_code == 200:
            break
        else:
            continue

def wait_until_sync(uri=URI_ON_TIME):
    while True:
        res = requests.get(uri)

        if res.status_code == 200 and int(traci.simulation.getTime()) <= int(res.json()["time"]):
            break
        else:
            continue
