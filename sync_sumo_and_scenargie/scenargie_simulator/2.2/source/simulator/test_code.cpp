#include <iostream>
#include <stdio.h>
#include <vector>
#include "common_library.h"

int SIMULATION_TIME = 100;

int main(){
  for ( int t = 0; t < SIMULATION_TIME; t++){
    printf("time : %d\n", t);

    int node_id = 1;
    double tmp = 0.0;
    receive_vehicle_data(node_id, tmp, tmp, tmp, double(t));
    std::vector<int> deleted_nodes = delete_vehicle();
    for (int i = 0; i < deleted_nodes.size(); i++) {
        std::cout << deleted_nodes[i] << std::endl;
    }
  }

  return 0;
}
