#include "my_headers.h"
#include <sstream>
#include <iostream>
#include <vector>
#include <map>

// ----- struct data -----
struct vehicle_data {
  int node_id;
  double access_time;
  double pos_x;
  double pos_y;
  double speed;
};

// ----- GLOBAL VARS -----
int SUMO_TIME_STEP = 1.0;
std::map<int, int> MAP_OF_MOBILITY_ID_AND_NODE_ID;
std::map<int, vehicle_data> MAP_OF_NODE_ID_AND_LAST_VEHICLE_DATA;

// ----- prototype assertion -----
std::vector<int> int_vector_from_lua_table(lua_State* l);

// ----- function -----
std::vector<int> delete_vehicle() {
  std::vector<int> deleted_nodes;

  // ----- premise for lua -----
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  luaL_loadfile(L, "test.lua");
  lua_pcall(L,0,0,0);

  // ----- call function in lua code. -----
  lua_getglobal(L, "delete_vehicle");
  lua_pcall(L, 0, 1, 0);

  deleted_nodes = int_vector_from_lua_table(L);
  return deleted_nodes;
}

std::vector<int> int_vector_from_lua_table(lua_State* l){
  std::vector<int> results;

  lua_pushnil(l);
  while (lua_next(l, -2)) {
    switch (lua_type(l, -1)) {  // value へのアクセス
    case LUA_TNUMBER :
      results.push_back(lua_tointeger(l, -1));
      break;
    default :
      printf("value=%s\n", lua_typename(l, lua_type(l, -1)));
    }

    lua_pop(l, 1);  // 値を取り除く
  }

  return results;
}

int get_node_id_by_mobility_id(int mobility_id) {
  auto iter = MAP_OF_MOBILITY_ID_AND_NODE_ID.find(mobility_id);

  if (iter != MAP_OF_MOBILITY_ID_AND_NODE_ID.end()) {
    return iter->second;
  } else {
    return -1;
  }
}

void save_map_of_mobility_id_and_node_id(int mobility_id, int node_id) {
  MAP_OF_MOBILITY_ID_AND_NODE_ID.insert(std::make_pair(mobility_id, node_id));
}

void send_current_time(double current_time) {
  // ----- premise for lua -----
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  luaL_loadfile(L, "test.lua");
  lua_pcall(L,0,0,0);

  // ----- call function in lua code. -----
  lua_getglobal(L, "send_current_time");
  lua_pushnumber(L, current_time);
  lua_pcall(L, 1, 0, 0);
}

void send_add_node(int node_id, double current_time) {
  // ----- premise for lua -----
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  luaL_loadfile(L, "test.lua");
  lua_pcall(L,0,0,0);

  // ----- call function in lua code. -----
  lua_getglobal(L, "send_add_node");
  lua_pushnumber(L, node_id);
  lua_pushnumber(L, current_time);
  lua_pcall(L, 2, 0, 0);
}

void send_received_packet(
  int receiver_id,
  double current_time,
  int sender_id,
  double sender_pos_x,
  double sender_pos_y,
  double sender_velocity,
  double send_time
) {
  // ----- premise for lua -----
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  luaL_loadfile(L, "test.lua");
  lua_pcall(L,0,0,0);

  // ----- call function in lua code. -----
  lua_getglobal(L, "send_received_packet");
  lua_pushnumber(L, receiver_id);
  lua_pushnumber(L, current_time);
  lua_pushnumber(L, sender_id);
  lua_pushnumber(L, sender_pos_x);
  lua_pushnumber(L, sender_pos_y);
  lua_pushnumber(L, sender_velocity);
  lua_pushnumber(L, send_time);
  lua_pcall(L, 7, 0, 0);
}

void send_is_finish() {
  // ----- premise for lua -----
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  luaL_loadfile(L, "test.lua");
  lua_pcall(L,0,0,0);

  // ----- call function in lua code. -----
  lua_getglobal(L, "send_is_finish");
  lua_pcall(L, 0, 0, 0);
}

void update_vehicle_state(
  double& pos_x,
  double& pos_y,
  double& current_speed,
  vehicle_data new_vehicle_state
) {
  pos_x = new_vehicle_state.pos_x;
  pos_y = new_vehicle_state.pos_y;
  current_speed = new_vehicle_state.speed;
}

void wait_until_sync(double current_time) {
  // ----- premise for lua -----
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  luaL_loadfile(L, "test.lua");
  lua_pcall(L,0,0,0);

  // ----- call function in lua code. -----
  lua_getglobal(L, "wait_until_sync");
  lua_pushnumber(L, current_time);
  lua_pcall(L, 1, 0, 0);
}

void receive_vehicle_data(
  int& node_id,
  double& pos_x,
  double& pos_y,
  double& current_speed,
  double current_time
){
  // ----- if current_time is less or equal to the last access time, -----
  // ----- remain vehicle state. -----
  auto iter = MAP_OF_NODE_ID_AND_LAST_VEHICLE_DATA.find(node_id);
  if (iter != MAP_OF_NODE_ID_AND_LAST_VEHICLE_DATA.end() && current_time - iter->second.access_time < SUMO_TIME_STEP) {
    update_vehicle_state(pos_x, pos_y, current_speed, iter->second);
    return;
  }

  // ----- premise for lua -----
  lua_State *L = luaL_newstate();
  luaL_openlibs(L);
  luaL_loadfile(L, "test.lua");
  lua_pcall(L,0,0,0);

  // ----- call function in lua code. -----
  lua_getglobal(L, "receive_vehicle_data");
  lua_pushnumber(L, node_id);
  lua_pcall(L, 1, 7, 0);

  // ----- if status code = 406, nothing is conducted. -----
  if (lua_tointeger(L, -7) == 406) {
    return;
  }

  // ----- save current access log -----
  vehicle_data new_vehicle_data = {
    node_id,
    current_time,
    lua_tonumber(L, -4),
    lua_tonumber(L, -3),
    lua_tonumber(L, -2),
  };

  if (MAP_OF_NODE_ID_AND_LAST_VEHICLE_DATA.count(node_id) == 0) {
    MAP_OF_NODE_ID_AND_LAST_VEHICLE_DATA.insert(std::make_pair(node_id, new_vehicle_data));
  } else {
    MAP_OF_NODE_ID_AND_LAST_VEHICLE_DATA[node_id] = new_vehicle_data;
  }

  update_vehicle_state(pos_x, pos_y, current_speed, new_vehicle_data);
}
