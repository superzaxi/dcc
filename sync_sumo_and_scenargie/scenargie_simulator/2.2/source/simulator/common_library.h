#include "my_headers.h"
#include <vector>

// ----- function -----
void save_map_of_mobility_id_and_node_id(int mobility_id, int node_id);

int get_node_id_by_mobility_id(int mobility_id);

void send_add_node(int node_id, double current_time);

void send_is_finish();

void send_current_time(double current_time);

void send_received_packet(
  int receiver_id,
  double current_time,
  int sender_id,
  double sender_pos_x,
  double sender_pos_y,
  double sender_velocity,
  double send_time
);

void wait_until_sync(double current_time);

void receive_vehicle_data(
  int& node_id,
  double& pos_x,
  double& pos_y,
  double& current_speed,
  double current_time
);

std::vector<int> delete_vehicle();
