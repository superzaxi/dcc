-- library --
json = require "json"
client = require('httpclient').new()

-- GLOVAL VARS --
HOST = '127.0.0.1'
PORT = 8000
PORT_FOR_PACKET = 7900
PORT_FOR_TIME = 7800

URI_ON_ADD_VEHICLE = "http://" .. tostring(HOST) .. ":" .. tostring(PORT) .. "/add_vehicle"
URI_ON_DELETE_VEHICLE = "http://" .. tostring(HOST) .. ":" .. tostring(PORT) .. "/delete_vehicle"
URI_ON_FINISH = "http://" .. tostring(HOST) .. ":" .. tostring(PORT) .. "/finish"
URI_ON_PACKET = "http://" .. tostring(HOST) .. ":" .. tostring(PORT_FOR_PACKET) .. "/packet"
URI_ON_TIME = 'http://'.. tostring(HOST) .. ':' .. tostring(PORT_FOR_TIME) .. '/time'
URI_ON_VEHICLE = "http://" .. tostring(HOST) .. ":" .. tostring(PORT) .. "/vehicle"

-- function --
function hello()
  print("Hello!")
end

function send_add_node(node_id, current_time)
  res = client:post(URI_ON_ADD_VEHICLE, json.encode({node_id = node_id, time = current_time}))

  if res.code ~= 200 then
    send_add_node(node_id, current_time);
  end
end

function send_is_finish();
  res = client:post(URI_ON_FINISH, json.encode({name = "Scenargie", is_finish = 1}))

  if res.code ~= 200 then
    send_is_finish();
  end
end

function send_current_time(current_time)
  res = client:post(URI_ON_TIME, json.encode({name = "Scenargie", time = current_time}))

  if res.code ~= 200 then
    send_current_time(current_time)
  end
end

function send_received_packet(receiver_id, current_time, sender_id, sender_pos_x, sender_pos_y, sender_velocity, send_time)
  res = client:post(URI_ON_PACKET, json.encode({
    receiver_id = receiver_id,
    time = current_time,
    sender_id = sender_id,
    sender_pos_x = sender_pos_x,
    sender_pos_y = sender_pos_y,
    sender_velocity = sender_velocity,
    send_time = send_time
  }))

  if res.code ~= 200 then
    send_received_packet(receiver_id, current_time, sender_id, sender_pos_x, sender_pos_y, sender_velocity, send_time)
  end
end

function wait_until_sync(current_time)
  res = client:get(URI_ON_TIME)
  body = json.decode(res.body)

  if res.code ~= 200 or body["time"] < current_time then
    wait_until_sync(current_time)
  end
end

function delete_vehicle()
  res = client:get(URI_ON_DELETE_VEHICLE)
  body = json.decode(res.body)

  if res.code ~= 200 then
    delete_vehicle()
  end

  result = {}
  for index = 1, #body do
    table.insert(result, tonumber(body[index]["node_id"]))
  end

  return result
end

function receive_vehicle_data(node_id)
  res = client:get(URI_ON_VEHICLE .. "?node_id=" .. tostring(node_id))
  body = json.decode(res.body)

  if res.code ~= 200 and res.code ~= 406 then
    receive_vehicle_data(node_id)
  end

  return res.code, body["time"], body["road"], body["pos_x"], body["pos_y"], body["speed"], body["accel"]
end
