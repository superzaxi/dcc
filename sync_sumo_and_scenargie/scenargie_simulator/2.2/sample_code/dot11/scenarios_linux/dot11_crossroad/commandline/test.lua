-- library --
json = require "json"
client = require('httpclient').new()

-- GLOVAL VARS --
HOST = '127.0.0.1'
PORT = 8000

URI_ON_ADD_VEHICLE = "http://" .. tostring(HOST) .. ":" .. tostring(PORT) .. "/add_vehicle"
URI_ON_DELETE_VEHICLE = "http://" .. tostring(HOST) .. ":" .. tostring(PORT) .. "/delete_vehicle"
URI_ON_FINISH = "http://" .. tostring(HOST) .. ":" .. tostring(PORT) .. "/finish"
URI_ON_TIME = 'http://'.. tostring(HOST) .. ':' .. tostring(PORT) .. '/time'
URI_ON_VEHICLE = "http://" .. tostring(HOST) .. ":" .. tostring(PORT) .. "/vehicle"

-- function --
function hello()
  print("Hello!")
end

function send_add_node(node_id, current_time)
  res = client:post(URI_ON_ADD_VEHICLE, json.encode({node_id = node_id, time = current_time}))

  if res.code ~= 200 then
    send_add_node(node_id, current_time);
  end
end

function send_is_finish();
  res = client:post(URI_ON_FINISH, json.encode({name = "Scenargie", is_finish = 1}))

  if res.code ~= 200 then
    send_is_finish();
  end
end

function send_current_time(current_time)
  res = client:post(URI_ON_TIME, json.encode({name = "Scenargie", time = current_time}))

  if res.code ~= 200 then
    send_current_time(current_time)
  end
end

function wait_until_sync(current_time)
  res = client:get(URI_ON_TIME)
  body = json.decode(res.body)

  if res.code ~= 200 or body["time"] < current_time then
    wait_until_sync(current_time)
  end
end
