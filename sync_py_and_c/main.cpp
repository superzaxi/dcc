#include <iostream>
#include "picojson.h"
#include "httplib.h"

// ##### GLOBAL VARS #####
int SIMULATION_TIME = 1000;
// static std::string URI = "http://127.0.0.1:8000/time";
static std::string HOST = "127.0.0.1";
int PORT = 8000;

// ##### function #####
picojson::object& json_to_obj(std::string json_string) {
  picojson::value v;
  const std::string err = picojson::parse(v, json_string);

  return v.get<picojson::object>();
}

void wait_until_sync(int current_time) {
  httplib::Client cli(HOST, PORT);
  auto res = cli.Get("/time");
  picojson::object& obj = json_to_obj(res->body);

  if ( int(res->status) != 200 || int(obj["time"].get<double>()) < int(current_time) ) {
    wait_until_sync(current_time);
  }
}

// ##### main #####
int main(){
  httplib::Client cli(HOST, PORT);

  for ( int t = 0; t < SIMULATION_TIME; t++){
    printf("time : %d\n", t);
    // ----- 現在時刻をシミュレータ同期用サーバに送信 -----
    auto res = cli.Post(
      "/time",
      "{\"name\": \"scenagie\", \"time\": " + std::to_string(t) + "}",
      "application/json"
    );
    std::cout << res->status << std::endl;

    // ----- 以下の条件を満たすまで待機 -----
    // ----- 1. 通信に成功する(200)
    // ----- 2. 返却された時刻がシミュレータ内の時刻と同じになる
    wait_until_sync(t);
  }

  return 0;
}
