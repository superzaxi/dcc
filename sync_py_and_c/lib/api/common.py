# coding : utf-8

import falcon
import json

class Common:
    def __init__(self):
        self.data = []

    def on_get(self, req, res):
        pass

    def on_post(self, req, res):
        try:
            body = req.bounded_stream.read()
            data = json.loads(body.decode('utf-8'))
            self.data.append(data)
            res.status = falcon.HTTP_200
        except Exception as e:
            print(e)
            res.status = falcon.HTTP_500
