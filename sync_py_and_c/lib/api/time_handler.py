# coding : utf-8

import falcon
import json

from common import (
    Common
)

class TimeHandler(Common):
    def max_time_by_name(self, name):
        try:
            return max([int(d["time"]) for d in self.data if str(d["name"]) == str(name)])
        except Exception as e:
            print(e)
            return 0

    def on_get(self, req, res):
        try:
            names = list(set([d["name"] for d in self.data]))
            if len(names) <= 1:
                raise Exception("only 1 name in data")
            else:
                res.body = json.dumps({
                    "time" : min([self.max_time_by_name(name) for name in names])
                })
                res.status = falcon.HTTP_200
        except Exception as e:
            print(e)
            res.body = json.dumps({"time" : 0})
            res.status = falcon.HTTP_500
