# coding : utf-8

import falcon
import json

from common import (
    Common
)

class VehicleHandler(Common):
    def on_get(self, req, res):
        params = req.params

        for d in self.data:
            if d["id"] == params["id"]:
                res.body = json.dumps(d)

                