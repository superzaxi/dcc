# coding : utf-8
import falcon
import json

from lib.api.packet_handler import (
    PacketHandler
)
from lib.api.time_handler import (
    TimeHandler
)
from lib.api.vehicle_handler import (
    VehicleHandler
)
from wsgiref import (
    simple_server
)

##### GLOBAL VARS #####
HOST = "127.0.0.1"
PORT = 8000

##### main #####
if __name__ == "__main__":
    packet_handler = PacketHandler()
    time_handler = TimeHandler()
    vehicle_handler = VehicleHandler()

    app = falcon.API()
    app.add_route("/packet", packet_handler)
    app.add_route("/time", time_handler)
    app.add_route("/vehicle", vehicle_handler)

    httpd = simple_server.make_server(HOST, PORT, app)
    httpd.serve_forever()
