# coding: utf-8

import json
import requests
import time

##### GLOBAL VARS #####
SIMULATION_TIME = 1000
URI = "http://127.0.0.1:8000/time"
SLEEP_TIME = 1

##### main #####
if __name__ == '__main__':
    for t in range(0, SIMULATION_TIME):
        print('time : {:}'.format(str(t)))
        time.sleep(SLEEP_TIME)
        # ----- 現在時刻をシミュレータ同期用サーバに送信 -----
        requests.post(URI, data=json.dumps({"name" : "SUMO", "time" : str(t)}))

        # ----- 以下の条件を満たすまで待機 -----
        # ----- 1. 通信に成功する(200)
        # ----- 2. 返却された時刻がシミュレータ内の時刻と同じになる
        res = requests.get(URI)
        while res.status_code != 200 or int(res.json()["time"] < int(t)):
            time.sleep(0.1)
            res = requests.get(URI)
            