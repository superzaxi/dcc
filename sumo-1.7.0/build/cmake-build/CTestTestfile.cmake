# CMake generated Testfile for 
# Source directory: /Users/yoshida/gitlab/mos-its-group/sumo-1.7.0
# Build directory: /Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/build/cmake-build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(exampletest "/usr/bin/python" "/Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/docs/examples/runAll.py")
set_tests_properties(exampletest PROPERTIES  _BACKTRACE_TRIPLES "/Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/CMakeLists.txt;568;add_test;/Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/CMakeLists.txt;0;")
subdirs("src")
