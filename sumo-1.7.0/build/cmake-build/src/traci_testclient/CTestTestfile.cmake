# CMake generated Testfile for 
# Source directory: /Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/src/traci_testclient
# Build directory: /Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/build/cmake-build/src/traci_testclient
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(libsumotest "/Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/bin/testlibsumo")
set_tests_properties(libsumotest PROPERTIES  _BACKTRACE_TRIPLES "/Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/src/traci_testclient/CMakeLists.txt;18;add_test;/Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/src/traci_testclient/CMakeLists.txt;0;")
add_test(libsumostatictest "/Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/bin/testlibsumostatic")
set_tests_properties(libsumostatictest PROPERTIES  _BACKTRACE_TRIPLES "/Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/src/traci_testclient/CMakeLists.txt;23;add_test;/Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/src/traci_testclient/CMakeLists.txt;0;")
