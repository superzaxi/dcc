# CMake generated Testfile for 
# Source directory: /Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/src/utils/gui
# Build directory: /Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/build/cmake-build/src/utils/gui
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("div")
subdirs("events")
subdirs("globjects")
subdirs("images")
subdirs("settings")
subdirs("tracker")
subdirs("windows")
subdirs("cursors")
subdirs("shortcuts")
