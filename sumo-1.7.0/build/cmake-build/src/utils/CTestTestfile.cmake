# CMake generated Testfile for 
# Source directory: /Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/src/utils
# Build directory: /Users/yoshida/gitlab/mos-its-group/sumo-1.7.0/build/cmake-build/src/utils
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("common")
subdirs("distribution")
subdirs("emissions")
subdirs("geom")
subdirs("importio")
subdirs("iodevices")
subdirs("options")
subdirs("shapes")
subdirs("router")
subdirs("traci")
subdirs("traction_wire")
subdirs("vehicle")
subdirs("xml")
subdirs("foxtools")
subdirs("gui")
